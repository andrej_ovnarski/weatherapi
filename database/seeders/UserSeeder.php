<?php

namespace Database\Seeders;

use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Support\Facades\Hash;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        DB::table('users')->insert([
            'name' => 'Jack',
            'email' => 'jack@example.com',
            'password' => Hash::make('12345678'),
            'city' => 'Bitola',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()
        ]);
    }
}
