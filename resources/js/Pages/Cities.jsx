import TextInput from '@/Components/TextInput';
import { useEffect, useState } from 'react';
import axios from 'axios';
import { Link } from '@inertiajs/react';

export default function Cities({ }) {
    const [message, setMessage] = useState('')
    const handleChange = (event) => {
        setMessage(event.target.value);
    }
    function getClassName(active) {
        if (active) {
            return "mr-1 mb-1 px-4 py-3 text-sm leading-4 border rounded focus:border-primary focus:text-primary bg-blue-700 text-white";
        } else {
            return "mr-1 mb-1 px-4 py-3 text-sm leading-4 border rounded hover:bg-white focus:border-primary focus:text-primary";
        }
    }
    const [data, setData] = useState({})
    const [page, setPage] = useState(1)
    useEffect(() => {
        axios.get(route('cities'), { params: { 'city': message, 'page': page } })
            .then((response) => setData(response.data))
    }, [page, message])
    
    const onPrevious = (event) => {
        let current_page = event.target.id
        let previous_page = (parseInt(current_page) - 1)
        setPage(previous_page)
    }
    const onNext = (event) => {
        let current_page = event.target.id
        let next_page = (parseInt(current_page) + 1)
        setPage(next_page)
    }
    return (
        <div>
            <div className='flex justify-between items-center'>
                <div className="p-6 text-gray-900">All cities</div>
                <div className='p-6 text-gray-900'>
                    <TextInput
                        name='city'
                        id='city'
                        value={message}
                        placeholder='Search'
                        onChange={handleChange}
                    />
                </div>
            </div>
            <div>
                <div className="py-8 mx-12">
                    <table className="w-full bg-white mb-6">
                        <thead className="bg-gray-800 text-white">
                            <tr>
                                <th className="w-1/3 text-left py-3 px-4 uppercase font-semibold text-sm text-center">ID</th>
                                <th className="w-1/3 text-left py-3 px-4 uppercase font-semibold text-sm text-center">Name</th>
                                <th className="w-1/3 text-left py-3 px-4 uppercase font-semibold text-sm text-center">City</th>
                            </tr>
                        </thead>
                        <tbody className="text-gray-700">
                            {
                                data.data?.map((data) => {
                                    return (
                                        <tr key={data.id}>
                                            <td className='text-center border px-4 py-2'>{data.id}</td>
                                            <td className='text-center border px-4 py-2'>{data.name}</td>
                                            <td className='text-center border px-4 py-2'>{data.city}</td>
                                        </tr>
                                    )
                                })}
                        </tbody>
                    </table>
                    { page === 1 ?
                    <Link
                        disabled
                        type='button'
                        as='button'
                        className={getClassName()}
                    >x</Link> :
                    <Link
                        type='button'
                        preserveState
                        preserveScroll
                        as='button'
                        className={getClassName()}
                        onClick={onPrevious}
                        id={data.current_page}
                    >&laquo;</Link>
                
                    }
                    
                    {data.last_page === page ?
                        <Link
                            disabled
                            type='button'
                            as='button'
                            className={getClassName()}
                        >x</Link> :
                        <Link
                            type='button'
                            as='button'
                            className={getClassName()}
                            onClick={onNext}
                            preserveState
                            preserveScroll
                            id={data.current_page}
                        >&raquo;</Link>
                    }

                </div>
            </div>
        </div>
    );
}