import AuthenticatedLayout from '@/Layouts/AuthenticatedLayout';
import Cities from './Cities';
import { Head } from '@inertiajs/react';

export default function Dashboard({ auth, temperature, feelsLike, forecast }) {
    return (
        <AuthenticatedLayout
            user={auth.user}
            header={<h2 className="font-semibold text-xl text-gray-800 leading-tight">Dashboard</h2>}
        >
            <Head title="Dashboard" />

            <div className="py-12">
                <div className="max-w-7xl mx-auto sm:px-6 lg:px-8">
                    <div className="bg-white overflow-hidden shadow-sm sm:rounded-lg">
                        <div className="p-6 text-gray-900">Welcome {auth.user['name']}, here's the weather forecast in {auth.user['city']}:</div>
                        <div className="p-6 text-gray-900"> 
                            <div>Weather: {forecast}</div>
                            <div>Temperature: {temperature}° F</div>
                            <div>Feels like: {feelsLike}° F</div>
                        </div>
                     <Cities/> 
                    </div>
                </div>
            </div>
        </AuthenticatedLayout>
    );
}
