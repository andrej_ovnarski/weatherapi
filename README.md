# Task 1

# GitLab Repository
Step 1: Clone the Repository
Use the following command to create a local copy on your machine:
```bash
git clone git@gitlab.com:andrej_ovnarski/weatherapi.git
```
Step 2: Navigate to the Repository Directory
Change your current working directory to the newly cloned repository:
```bash 
cd weatherapi
```
Step 3: Fetch the Latest Changes
To ensure you have the latest information about remote branches, use the following command:
```bash 
git fetch
```
Step 4: Pull the Desired Branch
Now, you can pull the branch you want to update:
```bash 
git pull origin main
```

# Set up Laravel project
Prerequisites
Before you begin, make sure you have the following software installed:

PHP (at least version 7.4)
Composer (https://getcomposer.org/)

Environment Configuration:
Create a copy of the .env.example file and rename it to .env. Update the necessary environment variables such as database connection details, app keys, etc.
```bash
cp .env.example .env
```

# Docker

Prerequisites:

Docker installed on your system (https://www.docker.com/get-started)

Step 1: Docker Structure

Create a docker folder with the following structure:

```scss
    docker-compose.yml
    docker/
  └── nginx/
      └── default.conf
      └── Dockerfile
  └── php/
      └── Dockerfile
```

Step 2: Run the Docker Containers

Open a terminal or command prompt, navigate to your project folder, and execute the following command:

```bash
    docker-compose up -d 
```

This will start the containers in detached mode, allowing them to run in the background. The -d flag stands for detached mode.

Step 3: Access Your Application

Your web application is now accessible through your web browser at http://localhost.

To stop the containers, run:
```bash
    docker-compose down
```
To remove the containers and associated volumes, run:
```bash
    docker-compose down --volumes
```
Make sure to secure your MySQL credentials and never use production credentials in this setup as-is. Use environment variables and secrets management in a real production environment.

Run in Docker PHP container to install all dependecies:
```bash
    composer install
```

Run in Docker PHP container to install node modules:
```bash
    npm install
```
# Set up and run API locally
Prerequisites
Before you begin, make sure you have generated API Key from `https://openweathermap.org/`

Step 1: Register on OpenWeatherMap

Step 2: Generate API Key:
Open your profile after the registration and select My API Keys.
Generate the key. Now you have access the API data from OpenWeatherMap.
Save the Key in environment folder.

Step 3: API endpoints:
Use the defined API routes from `routes/api.php` file and check if everything is working on the development server.

Step 4: Testing API: 
To test your API, you can use tools like Postman or Insomnia. Use these tools to send HTTP requests to your API endpoints and check the responses.

Step 1a Creating requests:
Click on the "New" button to create a new request.
Enter a name for the request.
Choose the HTTP method (e.g., GET, POST, PUT, DELETE).
Enter the request URL and parameters, if any.
Add request headers, authentication, and body (if applicable) as needed.

Step 2a Sending Requests:
Once you've set up the request, click the "Send" button to send the request to the API server.
Observe the response in the "Body" tab. The server's response will be displayed here.

Step 3a Analyzing Responses:
Check the response status code to ensure the request was successful (200 series status codes) or to identify any errors (e.g., 4xx or 5xx status codes).
Inspect the response body for the data returned by the API.
Review response headers for additional information.

# Making tests with Pest 

Step 1: Installation:
You can run your tests by executing the pest command.
```bash
./vendor/bin/pest
```
Step 2: Testing:
Write your API tests: In your test file, you can now write Pest tests for your API routes. Pest's syntax is straightforward. Here's an example of testing a simple API endpoint:
```php
    it('can show city from api', function () {
    $token = 'Your generated token from Auth Bearer Token';
    $response = $this->withHeaders(['Authorization' => 'Bearer '.$token, 'Accept' => 'application/json'])->getJson('http://localhost/api/weather/city?city=london');
    $response->assertStatus(200);
});
```
In this example, we are testing if the API endpoint returns the same name with the city that we've typed.


# Laravel Sanctum

Run your database migrations. Sanctum will create one database table in which to store API tokens:

```bash
    php artisan migrate
```

After creating a access token for the user. Go to Postman and use the following headers for the endpoints:

'headers' => [
    'Authorization' => 'Bearer '.$token,
]

# Laravel Breeze 

Step 1: Run your database migrations. 

```bash
    php artisan migrate
```

Optional step: After the migrations run your database seeders to fill your database:
```bash
    php artisan db:seed
```

Step 3: After installed node modules and migrated database in Docker PHP container run:

```bash
    npm run dev
```
This will run the development server.

Next, you may navigate to your application's `/login`or `/register` URLs in your web browser. All of Breeze's routes are defined within the `routes/auth.php` file.