<?php

use Tests\TestCase;

uses(TestCase::class);
it('not working for unauthenticated users', function () {
    $response = $this->getJson('http://localhost/api/weather/coordinates?lat=51.5073219&lon=-0.1276474');
    $response->assertStatus(401);
});
it('can show weather from coordinates api', function () {
    $token = '6En3mdlwIovcUifP01QRzawsiXxHl8vgZautMzxY';
    $response = $this->withHeaders(['Authorization' => 'Bearer '.$token, 'Accept' => 'application/json'])->getJson('http://localhost/api/weather/coordinates?lat=51.5073219&lon=-0.1276474');
    $response->assertStatus(200);
});
