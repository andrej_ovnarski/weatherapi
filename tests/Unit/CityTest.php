<?php

use Tests\TestCase;

use GuzzleHttp\Client;
use function PHPUnit\Framework\assertSame;

uses(TestCase::class);
it('not working for unauthenticated users', function () {
    $response = $this->getJson('http://localhost/api/weather/city?city=london');
    $response->assertStatus(401);
});
it('can show city from api', function () {
    $token = '6En3mdlwIovcUifP01QRzawsiXxHl8vgZautMzxY';
    $response = $this->withHeaders(['Authorization' => 'Bearer '.$token, 'Accept' => 'application/json'])->getJson('http://localhost/api/weather/city?city=london');
    $response->assertStatus(200);
});
it('cant find the city', function () {
    $token = '6En3mdlwIovcUifP01QRzawsiXxHl8vgZautMzxY';
    $response = $this->withHeaders(['Authorization' => 'Bearer '.$token, 'Accept' => 'application/json'])->getJson('http://localhost/api/weather/city?city=londonasdfsaf');
    $response->assertStatus(404);
});