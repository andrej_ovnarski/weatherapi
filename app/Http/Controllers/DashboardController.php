<?php

namespace App\Http\Controllers;

use App\Models\User;
use Inertia\Inertia;
use Illuminate\Http\Request;
use App\Services\WeatherService;
use Illuminate\Support\Facades\Auth;

class DashboardController extends Controller
{
    private WeatherService $weatherService;

    public function __construct(WeatherService $weatherService)
    {
        $this->weatherService = $weatherService;
    }

    public function index(){
        $user = Auth::user();
        $city = $this->weatherService->getCoordinatesByCity($user->city);
        $body = json_decode($city->getBody()->getContents(), true);
        $coordinates = $body[0];
        $weather = $this->weatherService->getWeatherByCoordinates($coordinates['lat'], $coordinates['lon']);
        $weatherData = json_decode($weather->getBody()->getContents(), true);
        $tempreture = $weatherData['main']['temp'];
        $feelsLike = $weatherData['main']['feels_like'];
        $forecast = $weatherData['weather'][0]['main'];
        return Inertia::render('Dashboard', [
            'temperature' => $tempreture,
            'feelsLike' => $feelsLike,
            'forecast' => $forecast
        ]);
    }

    public function cities(Request $request){
        $users = User::when($request->city, function($query, $city){
            $query->where('city', 'LIKE', $city . '%');
        })->paginate(2);
        return $users;
    }
}
