<?php

namespace App\Http\Controllers\Api;

use App\Models\User;
use App\Traits\HttpResponses;
use App\Http\Requests\AuthRequest;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use App\Http\Requests\LoginAuthRequest;

class AuthController extends Controller
{
    use HttpResponses;

    public function register(AuthRequest $request){
            $user = new User();
            $user->name = $request->name;
            $user->email = $request->email;
            $user->password = Hash::make($request->password);
            if(!$user->save()){
                return $this->error('Something went wrong');
            }
            $user->save();
            $token = $user->createToken('token')->plainTextToken;
            return $this->success([
                'token' => $token
            ],'User created successfully');
            
    }

    public function login(LoginAuthRequest $request){
        if(!Auth::attempt($request->only(['email', 'password']))){
            return $this->error('Wrong Credentials', 401);
        }
        $user = User::where('email', $request->email)->first();
        $token = $user->createToken('token')->plainTextToken;
        return $this->success([
            'token' => $token,
        ], 'Logged in successfully'); 
       
    }

    public function logout(LoginAuthRequest $request){
        if(!Auth::attempt($request->only(['email', 'password']))){
            return $this->error('Wrong Credentials', 401);
        }
        $user = User::where('email', $request->email)->first();
        $user->tokens()->delete();
        return $this->success('', 'User logged out');
    }
}
