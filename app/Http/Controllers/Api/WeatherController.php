<?php

namespace App\Http\Controllers\Api;

use App\Services\WeatherService;
use App\Http\Requests\CityRequest;
use App\Http\Controllers\Controller;
use App\Http\Requests\CoordinatesRequest;

class WeatherController extends Controller
{
    private WeatherService $weatherService;

    public function __construct(WeatherService $weatherService)
    {
        $this->weatherService = $weatherService;
    }

    public function coordinates(CoordinatesRequest $request)
    {
        $lat = $request->input('lat');
        $lon = $request->input('lon');
        $response = $this->weatherService->getWeatherByCoordinates($lat, $lon);
        if ($response == null) {
            return response()->json(['error' => 'Could not resolve host', 'status' => 500]);
        }
        $body = json_decode($response->getBody()->getContents(), true);
        return response()->json([
            "name" => $body['name'],
            "coordinates" => $body['coord'],
            "weather" => $body['weather'],
            "forecast" => $body['main']
        ]);
        return response()->json(['error' => 'Something wrong', 'status' => 404], 404);
    }

    public function city(CityRequest $request)
    {
        $city = ucfirst($request->input('city'));
        $response = $this->weatherService->getCoordinatesByCity($city);
        if ($response == null) {
            return response()->json(['error' => 'Could not resolve host', 'status' => 500]);
        }
        $body = json_decode($response->getBody()->getContents(), true);
        if ($body != null) {
            $data = $body[0];
            return response()->json([
                "name" => $data['name'],
                "lat" => $data['lat'],
                "lon" => $data['lon']
            ]);
        }
        return response()->json(['error' => 'City not found', 'status' => 404], 404);
    }
}
