<?php

namespace App\Clients;

use GuzzleHttp\Client;


class WeatherClient
{

    public function client()
    {
        $client = new Client([
            'base_uri' => 'https://api.openweathermap.org/'
        ]);
        return $client;
    }
}
