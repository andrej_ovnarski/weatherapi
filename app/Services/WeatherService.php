<?php

namespace App\Services;

use App\Clients\WeatherClient;
use Throwable;

class WeatherService
{
    private WeatherClient $weatherClient;

    public function __construct(WeatherClient $weatherClient)
    {
        $this->weatherClient = $weatherClient;
    }

    public function getCoordinatesByCity($city)
    {
        $api_key = config('app.api_key');
        try {
            $client = $this->weatherClient->client();
            $response = $client->request('GET', 'geo/1.0/direct', [
                'query' => [
                    'q' => $city,
                    'appid' => $api_key
                ]
            ]);
        } catch (Throwable $e) {
            return $e->getCode();
        }
        return $response;
    }

    public function getWeatherByCoordinates($lat, $lon)
    {
        $api_key = config('app.api_key');
        try {
            $client = $this->weatherClient->client();
            $response = $client->request('GET', 'data/2.5/weather', [
                'query' => [
                    'lat' => $lat,
                    'lon' => $lon,
                    'appid' => $api_key
                ]
            ]);
        } catch (Throwable $e) {
            return $e->getCode();
        }
        return $response;
    }
}
