<?php

namespace App\Exceptions;

use Throwable;
use Illuminate\Http\Exceptions\ThrottleRequestsException;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\HttpKernel\Exception\MethodNotAllowedHttpException;

class Handler extends ExceptionHandler
{
    /**
     * 
     * The list of the inputs that are never flashed to the session on validation exceptions.
     *
     * @var array<int, string>
     */
    protected $dontFlash = [
        'current_password',
        'password',
        'password_confirmation',
    ];

    /**
     * Register the exception handling callbacks for the application.
     */
    public function register(): void
    {
        $this->reportable(function (Throwable $e) {
            //
        });
    }
    
    public function render($request, Throwable $e){
        if($request->is('api/*')){
            if($e instanceof NotFoundHttpException){
                    return response()->json([
                        'message' => 'Not Found.',
                        'status' => 404], 404);
            }
            if($e instanceof ThrottleRequestsException){
                    return response()->json([
                        'message' => 'Too many requests.',
                        'status' => 429], 429);
            }
            if($e instanceof MethodNotAllowedHttpException){
                    return response()->json([
                        'message' => 'Unauthenticated.',
                        'status' => 401], 401);
            }
        }
        return parent::render($request, $e);   
    }
}
