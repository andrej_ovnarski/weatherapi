<?php

namespace App\Traits;


trait HttpResponses {
    protected function success($data, $message = null, $status = 200) {
        return response()->json([
            'status' => 'Request was successful',
            'message' => $message,
            'data' => $data
        ], $status);
    }

    protected function error($message = null, $status = 400) {
        return response()->json([
            'message' => $message
        ], $status);
    }
}